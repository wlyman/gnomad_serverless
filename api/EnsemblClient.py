import mysql.connector


class EnsemblClient:
    def __init__(
            self,
            host="ensembldb.ensembl.org",
            user="anonymous",
            database="homo_sapiens_core_75_37",
    ):
        self._cxn = mysql.connector.connect(
            host=host, user=user, database=database
        )
        self._query = """SELECT transcript.stable_id, xref.display_label
                   FROM transcript, object_xref, xref, external_db 
                   WHERE transcript.transcript_id = object_xref.ensembl_id 
                       AND object_xref.ensembl_object_type = 'Transcript' 
                       AND object_xref.xref_id = xref.xref_id 
                       AND xref.external_db_id = external_db.external_db_id 
                       AND external_db.db_name = 'RefSeq_mRNA'
                       AND dbprimary_acc = %s"""

    def __call__(self, refseq):
        refseq = refseq.split(".", 1)[0]

        cur = self._cxn.cursor(buffered=True)

        cur.execute(self._query, (refseq,))

        return list(cur)
