from urllib.parse import quote_plus
import requests
from typing import NamedTuple
from os import getenv

class BiotoolsAlterationsClient:
    def __init__(
            self,
            alterationsJSONFormat="{host}/api/alterations.json?alterations={alteration}",
    ):
        self.alterationsJSONFormat = alterationsJSONFormat

    def __call__(self, alteration, datasets=()):
        url = self.alterationsJSONFormat.format(
            host=getenv("BIO_API"),
            alteration=quote_plus(alteration)
        )
        if datasets:
            url += f"&datasets={','.join(datasets)}"
        result = requests.get(url).json()["alterations"][0]
        if not result["success"]:
            raise LookupError(f"Failed fetching: {result['alteration']} \n {result['error_message']}")
        
        return result

    def fetch_gene_range(self, accession):
        alt_data = self(accession + " c.1A>T")
        mrna = sorted(int(s) for s in alt_data["mrna_structure"].split(","))
        return GeneRange(alt_data["chr_display"], mrna[0], mrna[-1])


class GeneRange(NamedTuple):
    chromosome: str
    chr_start: int
    chr_end: int
