import logging

from .BioApiClient import BiotoolsAlterationsClient

import requests
from os import getenv

ALT_JSON_VCF_FIELDS = ("chromosome", "genomic_vcf_position", "genomic_vcf_ref_nt", "genomic_vcf_alt_nt")

logger = logging.getLogger("gunicorn.error")


def parse(alterations_string):

    alterations_data = BiotoolsAlterationsClient()(alterations_string)
    base_url = getenv("BIO_API_PY")
    url = f"{base_url}/api/v1.0/normalize_vcf"
    try:
        unnormalized_vcf_string = ":".join(str(alterations_data[k]) for k in ALT_JSON_VCF_FIELDS)
        result = requests.get(url, verify=False, params={"variant": unnormalized_vcf_string}).json()["api_data"]
    except Exception as e:
        logger.exception(e)
        return []

    return [
        alterations_data,
        result['chr'],
        result['pos'],
        result['ref'],
        result['alt'],
        alterations_data['gene']
    ]
