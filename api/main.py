import json
import os
from pathlib import Path
from .AlterationParser import parse
from .BioApiClient import BiotoolsAlterationsClient
from .EnsemblTest import test
from flask import Flask, Response, send_from_directory, request
from flask_caching import Cache
from dotenv import load_dotenv
from .gnomad import Gnomad
import subprocess
import requests
#from urllib.parse import unquote   #workaround for dev desting

load_dotenv(dotenv_path=Path('.') / '.env')

app = Flask(__name__)
#cache = Cache()

#cache.init_app(app,  {"CACHE_TYPE": "memcached", "CACHE_DEFAULT_TIMEOUT": 3600})

@app.route("/")
#@cache.cached()
def home():
    return "Test Message"
    

@app.route("/favicon.ico")
#@cache.cached()
def favicon():
    return send_from_directory(
        os.path.join(app.root_path, "static"),
        "favicon.ico",
        mimetype="image/vnd.microsoft.icon",
    )


@app.route("/test/")
def test():
    test = requests.get('http://internal-elb-bioapi-prod-340763953.us-west-2.elb.amazonaws.com//api/alterations.json?alterations=tp53+c.319T%3EC')
    #os.chdir('/tmp')
    #cmd1 = """ mv /htslib /tmp """
    #cmd_output2 = subprocess.getoutput(cmd1)
    #cmd2 = """ mv /bcftools /tmp """
    #cmd_output2 = subprocess.getoutput(cmd2)
    #cmd = """ ls /tmp"""
    #print (cmd)
    
    #cmd = """
    #    /tmp/htslib/tabix -h https://storage.googleapis.com/gcp-public-data--gnomad/release/2.1.1/vcf/exomes/gnomad.exomes.r2.1.1.sites.17.vcf.bgz 17:7579368-7579368 | 
    #    /tmp/bcftools/bcftools filter -i"
    #     """
    
    #cmd_output = subprocess.getoutput(cmd)
    return Response(json.dumps({'output': test.content}))

@app.route("/search/<alteration>")
#@cache.cached()
def gnomad_search_by_alt(alteration):
    #alteration = unquote(alteration)   #workaround for dev testing
    parsed = parse(alteration)
    if not parsed:
        return Response(json.dumps({'error': 'An error occurred while fetching the variant.'}), mimetype="application/json")

    [alteration_data, chromosome, pos, ref, alt, gene] = parsed

    if alteration_data.get('sub_category') == 'gross' or len(alteration_data.get('genomic_vcf_ref_nt')) > 10:
        return Response(json.dumps({'error': 'You have requested an unsupported variant type.'}))

    gnomad = Gnomad()
    res = gnomad.get_stats(gene, chromosome, pos, ref, alt)

    if request.args.get('merged', default=False, type=bool) and res:
        res = gnomad.combine_stats(res)
        res = gnomad.add_hw_test_results(res, gene)

    if request.args.get('vep', default=False, type=bool) and res:
        res["vep"] = gnomad.get_annotation(chromosome, pos, ref, alt)

    if request.args.get('ensembl', default=False, type=bool) and res:
        res['emsembl'] = test(res, alteration_data)

    return Response(
        json.dumps(res),
        mimetype="application/json",
    )


@app.route("/<nucleotide_id>")
#@cache.cached()
def gnomad_gene_search(nucleotide_id):
    gene = BiotoolsAlterationsClient().fetch_gene_range(nucleotide_id)
    test_data = json.dumps(
            Gnomad().get_range(gene.chromosome, gene.chr_start, gene.chr_end)
        )
    print (str(test_data), 'banana')
    return Response(
        json.dumps(
            Gnomad().get_range(gene.chromosome, gene.chr_start, gene.chr_end)
        ),
        mimetype="application/json",
    )


if __name__ == "__main__":
    app.config['DEBUG'] = True
    app.run(host='0.0.0.0', port=80) 
