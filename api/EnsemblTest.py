from .EnsemblClient import EnsemblClient
import re


def test(gnomadRaw, alterationsData):
    hgvsc = {
        f"{t.split('.', 1)[0]}:{v}"
        for t, v in (
        a["HGVSc"].replace(":n.", ":c.").split(":", 1)
        for a in gnomadRaw["vep"]
        if a["HGVSc"]
    )
    }

    ensemblHits = EnsemblClient()(alterationsData["nucleotide_id"])

    matching_annotations = []

    for e, _ in ensemblHits:
        query = f"{e.split('.', 1)[0]}:{_lowercase_ins_del_dup(alterationsData['nucleotide_alteration'])}"
        for h in hgvsc:
            if h == query:
                matching_annotations.append(query)
                break
    return matching_annotations


def _lowercase_ins_del_dup(varString):
    return _DEL_INS_DUP_RE.sub(_lower_match_group, varString)


def _lower_match_group(match):
    return match.group(1).lower()


_DEL_INS_DUP_RE = re.compile("(DEL|INS|DELINS|DUP)")
