from pathlib import Path
from os import getenv
import mysql.connector
import math
from scipy import stats
from statsmodels.stats.proportion import proportions_ztest


def fetch_cutoffs(gene_name):

    cnx = mysql.connector.connect(user=getenv('GENE_PAGE_DATABASE_USER'), password=getenv('GENE_PAGE_DATABASE_PWD'),
                                  host=getenv('GENE_PAGE_DATABASE_HOST'), database=getenv('GENE_PAGE_DATABASE_NAME'))
    cursor = cnx.cursor()

    query = """
    SELECT GROUP_CONCAT(distinct gim.model_name SEPARATOR ',') as inheritance_models, MAX(incidence), clinicalSensitivity
    FROM gene_inheritance_models as gim 
    LEFT JOIN gene_info as gi USING(`gene_id`) 
    LEFT JOIN gene_diseases gd USING(`gene_id`) 
    WHERE gi.gene_name = %s
    """
    cursor.execute(query, (gene_name,))

    (models, incidence, clinical_sensitivity) = cursor.fetchone()

    cursor.close()
    cnx.close()

    incidence = 0 if not incidence else incidence
    clinical_sensitivity = 1 if not clinical_sensitivity else clinical_sensitivity
    models = '' if not models else models

    incidence = int(incidence)
    clinical_sensitivity = float(clinical_sensitivity)
    models = models.split(',')

    if incidence > 0 and 'AR' in models:
        cutoff_hwAR = math.sqrt((1 / incidence) * clinical_sensitivity)
    else:
        cutoff_hwAR = 0

    if incidence > 0 and 'AD' in models:
        penetranceAD = .3
        cutoff_hwAD = 1 - math.sqrt(1 - clinical_sensitivity / (incidence * penetranceAD))
    else:
        cutoff_hwAD = 0
    return cutoff_hwAR, cutoff_hwAD



def hw_test(an, ac, cutoff_ar, cutoff_ad):

    flag = 0
    flag_ar = 0
    flag_ad = 0
    if not an or not ac:
        return flag, flag_ar, flag_ad
    (y, x) = (an, ac)
    n = y + x
    if n <= 0:
        return flag, flag_ar, flag_ad

    # Compute the pvalue
    # prop.test valid if both np0 and n(1-p0) >= 10
    if 0.05 * n >= 10:
        (_, pvalue) = proportions_ztest(x, n, value=0.05, alternative='larger', prop_var=.05)
    else:
        pvalue = stats.binom_test(x, n=n, p=0.05, alternative='greater')

    if 0.01 * n >= 10:
        (_, pvalue2) = proportions_ztest(x, n, value=0.01, alternative='larger', prop_var=.01)
    else:
        pvalue2 = stats.binom_test(x, n=n, p=0.01, alternative='greater')

    # Calculate C.I.
    if x >= 15 and (n - x) >= 15:
        p = x / n
        lower95 = p - abs(stats.norm.ppf(0.05)) * math.sqrt(p * (1 - p) / n)
    else:
        # Use Plus4 adjustment to compute the C.I.
        p = (x + 2) / (n + 4)
        lower95 = p - abs(stats.norm.ppf(0.05)) * math.sqrt(p * (1 - p) / (n + 4))

    # Common variants
    if pvalue <= 0.05 and lower95 >= 0.05:
        flag = 2

    # Intermediate variants
    elif pvalue2 <= 0.05 and lower95 >= 0.01:
        flag = 1

    # AR genes with available disease incidence
    if cutoff_ar > 0:
        # Compute the pvalue
        if cutoff_ar * n >= 10:
            (_, pvalue_ar) = proportions_ztest(x, n, value=cutoff_ar, alternative='larger',
                                               prop_var=cutoff_ar)
        else:
            pvalue_ar = stats.binom_test(x, n=n, p=cutoff_ar, alternative='greater')

        if pvalue_ar <= 0.05 and lower95 >= cutoff_ar and x > 3:
            flag_ar = 1

    if cutoff_ad > 0:
        # Compute the pvalue
        if cutoff_ad * n >= 10:
            (_, pvalue_ad) = proportions_ztest(x, n, value=cutoff_ad, alternative='larger',
                                               prop_var=cutoff_ad)
        else:
            pvalue_ad = stats.binom_test(x, n=n, p=cutoff_ad, alternative='greater')

        if pvalue_ad <= 0.05 and lower95 >= cutoff_ad and x > 3:
            flag_ad = 1

    return flag, flag_ar, flag_ad