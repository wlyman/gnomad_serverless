import subprocess
import os
from subprocess import CalledProcessError

from flask import current_app

from .hw_calc import fetch_cutoffs, hw_test

_IDENTIFIERS_FORMAT = ";".join(
    f"{name}:%{name.upper()}" for name in ("chrom", "pos", "ref", "alt")
)

_STR_FIELDS = ("popmax", "chrom", "ref", "alt")

valid_ref_alt = ["A", "T", "G", "C", "N"]

_VEP_FORMAT = "Allele|Consequence|IMPACT|SYMBOL|Gene|Feature_type|Feature|BIOTYPE|EXON|INTRON|HGVSc|HGVSp|cDNA_position|CDS_position|Protein_position|Amino_acids|Codons|Existing_variation|ALLELE_NUM|DISTANCE|STRAND|FLAGS|VARIANT_CLASS|MINIMISED|SYMBOL_SOURCE|HGNC_ID|CANONICAL|TSL|APPRIS|CCDS|ENSP|SWISSPROT|TREMBL|UNIPARC|GENE_PHENO|SIFT|PolyPhen|DOMAINS|HGVS_OFFSET|GMAF|AFR_MAF|AMR_MAF|EAS_MAF|EUR_MAF|SAS_MAF|AA_MAF|EA_MAF|ExAC_MAF|ExAC_Adj_MAF|ExAC_AFR_MAF|ExAC_AMR_MAF|ExAC_EAS_MAF|ExAC_FIN_MAF|ExAC_NFE_MAF|ExAC_OTH_MAF|ExAC_SAS_MAF|CLIN_SIG|SOMATIC|PHENO|PUBMED|MOTIF_NAME|MOTIF_POS|HIGH_INF_POS|MOTIF_SCORE_CHANGE|LoF|LoF_filter|LoF_flags|LoF_info"
_VEP_COLUMNS = _VEP_FORMAT.split("|")


class Gnomad:
    def __init__(self):
        pass

    def get_stats(self, gene, chromosome, pos, ref, alt):
        filter_include = f"POS={pos}"
        if ref[0] != "N" and alt[0] != "N":
            filter_include += f" && REF='{ref}' && ALT='{alt}'"

        output = {}
        if any(r not in valid_ref_alt for r in ref.upper()) or any(
            r not in valid_ref_alt for r in alt.upper()
        ):
            return output
        for data_set in ["exomes", "genomes"]:
            exomes = data_set == "exomes"
            filters = self.filter_string(exomes)

            os.chdir('/tmp')
            cmd1 = """ mv /htslib /tmp """
            cmd_output1 = subprocess.getoutput(cmd1)
            cmd2 = """ mv /bcftools /tmp """
            cmd_output2 = subprocess.getoutput(cmd2) 
            #/tmp/htslib/tabix -h https://storage.googleapis.com/gcp-public-data--gnomad/release/2.1.1/vcf/{dataset}/gnomad.{dataset}.r2.1.1.sites.{ch}.vcf.bgz {ch}:{pos}-{pos}|
            cmd = """
               /bcftools/bcftools view -r {ch}:{pos} /mnt/gnomad/gnomad.{dataset}.r2.1.1.sites.{ch}.vcf.bgz |
               /bcftools/bcftools filter -i "{filter_include}" |  
               /bcftools/bcftools query -f \"%REF\t%ALT\t{filters}\n\"
               """.format(
                dataset=data_set,
                ch=chromosome,
                pos=pos,
                filter_include=filter_include,
                filters=filters,
            )
            cmd_output = subprocess.getoutput(cmd)
            os.chdir('/home/lambda/api')
            if cmd_output == "":
                continue
            

            hits = [h.split("\t") for h in cmd_output.split("\n") if h]

            if ref[0] == "N" and alt[0] == "N":
                hits = [h for h in hits if h[0][1:] == ref[1:] and h[1][1:] == alt[1:]]

            if not hits:
                continue

            try:                
                assert len(hits) == 1
            except AssertionError as e:
                current_app.logger.error(hits)
                raise

            list_output = dict(
                self.convert_types(self.text_to_list(hits[0][2]))
            )
            output[data_set] = self.map_popmax_faf95(
                self.calculate_genotypes(list_output, exomes, chromosome)
            )
        return output

    def get_annotation(self, chromosome, pos, ref, alt):
        filter_include = f"POS={pos}"
        if ref[0] != "N" and alt[0] != "N":
            filter_include += f" && REF='{ref}' && ALT='{alt}'"
        output = []
        for data_set in ["exomes", "genomes"]:
            
            #/tmp/htslib/tabix -h https://storage.googleapis.com/gcp-public-data--gnomad/release/2.1.1/vcf/{data_set}/gnomad.{data_set}.r2.1.1.sites.{chromosome}.vcf.bgz {chromosome}:{pos}-{pos} | 
            #bcftools/bcftools view -r {chromosome}:{pos} /gnomad/gnomad.{data_set}.r2.1.1.sites.{chromosome}.vcf.bgz | 
            cmd = f"""
	           /bcftools/bcftools view -r {chromosome}:{pos} /mnt/gnomad/gnomad.{data_set}.r2.1.1.sites.{chromosome}.vcf.bgz |
               /bcftools/bcftools filter -i "{filter_include}" |  
               /bcftools/bcftools query -f \"%REF\t%ALT\t%vep\n\"
               """
            try:
                r = subprocess.run(
                    cmd, shell=True, text=True, check=True, capture_output=True
                )
            except CalledProcessError as e:
                current_app.logger.error(f"o: {e.stdout}\ne: {e.stderr}")
                raise

            if r.stdout:
                hits = [h.split("\t") for h in r.stdout.split("\n") if h]

                if ref[0] == "N" and alt[0] == "N":
                    hits = [h for h in hits if h[0][1:] == ref[1:] and h[1][1:] == alt[1:]]

                if hits:
                    assert len(hits) == 1
                    output.extend([
                        {n: v for n, v in zip(_VEP_COLUMNS, al)}
                        for al in (a.split("|") for a in hits[0][2].split(","))
                    ])

        return output

    def get_range(self, chromosome, start, end):
        output = {}
        print ('beginnning of function')
        for data_set in ["exomes", "genomes"]:
            exomes = data_set == "exomes"
            filters = self.filter_string(exomes)
            #f"/tmp/htslib/tabix -h https://storage.googleapis.com/gcp-public-data--gnomad/release/2.1.1/vcf/{data_set}/gnomad.{data_set}.r2.1.1.sites.{chromosome}.vcf.bgz {chromosome}:{start}-{end}"
            cmd = (
                f" /htslib/tabix -h /mnt/gnomad/gnomad.{data_set}.r2.1.1.sites.{chromosome}.vcf.bgz {chromosome}:{start}-{end}"
                f' | /bcftools/bcftools query -f "{_IDENTIFIERS_FORMAT};{filters}\n"'
            )
            list_output = subprocess.getoutput(cmd).split("\n")
            output[data_set] = [
                self.calculate_genotypes(
                    dict(self.convert_types(self.text_to_list(variant))), exomes, chromosome
                )
                for variant in list_output
            ]
        print ("I made it here")
        return output

    def populations(self, exomes, faf=False):
        pops = ["afr", "amr", "eas", "nfe"]
        if exomes:
            pops += ["sas"]
        if not faf:
            pops += ["asj", "fin", "oth", "female", "male"]
        return pops

    def filter_string(self, exomes):
        pops = self.populations(exomes)
        fields = ["AC", "AN", "AF", "nhomalt"]
        filters = [
            f"{field}_{pop}:%{field}_{pop}" for pop in pops for field in fields
        ]
        filters += [
            f"{field}_{pop}_male:%{field}_{pop}_male" for pop in pops for field in fields if pop != 'male' and pop != 'female' #add filter for other pops ie, afr_male
        ]
        filters += [f"{field}:%{field}" for field in fields]
        filters += [
            f"faf95_{pop}:%faf95_{pop}"
            for pop in self.populations(exomes, faf=True)
        ]
        filters += [
            "popmax:%popmax",
            "AC_popmax:%AC_popmax",
            "AN_popmax:%AN_popmax",
            "AF_popmax:%AF_popmax",
            "nhomalt_popmax:%nhomalt_popmax",
            "faf95:%faf95",
        ]
        return ";".join(filters)

    def text_to_list(self, out):
        return [
            split_ignore_empty(s, ":") for s in split_ignore_empty(out, ";")
        ]

    def map_popmax_faf95(self, structure):
        try:
            structure["faf95_popmax"] = max([structure.get("faf95_" + pop, 0) for pop in  self.populations(True)])
        except (KeyError, TypeError):
            pass
        return structure

    def calculate_genotypes(self, structure, exomes, chromosome):        
        for population in self.populations(exomes):            
            try:            
                ac = structure["AC_" + population]
                an = structure["AN_" + population]
                n_homo = structure["nhomalt_" + population]
                if chromosome == 'X' and population != 'male' and population != 'female':  #covers other populations ie, afr_male
                    n_hemi = structure["AC_" + population + "_male"]  
                elif chromosome == 'X' and population == 'male': 
                    n_hemi = structure["AC_" + population]      
                else: n_hemi = 0
            except KeyError:                
                continue

            n_het = ac - n_hemi - 2 * n_homo                        
            structure["nhetalt_" + population] = n_het
            structure["nwtalt_" + population] = an / 2 - n_het - n_homo
            structure["nhemialt_" + population] = n_hemi
            structure["totalt_" + population] = (
                n_het + n_homo + structure["nwtalt_" + population]
            )
        return structure

    def convert_types(self, structure):        
        return [
            [
                field,
                None
                if data == "."
                else float(data)
                if any(field.startswith(f) for f in ("AF", "faf95"))
                else data
                if field in _STR_FIELDS
                else int(data),
            ]
            for [field, data] in structure
        ]

    def combine_stats(self, stats):
        if not stats.get("genomes"):
            return self.convert_fafs(stats.get("exomes"), "exome")
        if not stats.get("exomes"):
            return self.convert_fafs(stats.get("genomes"), "genome")

        combined = {}
        for key in (key for key in stats["exomes"].keys() if "95" not in key):
            combined[key] = safe_add(
                stats["exomes"][key], stats["genomes"].get(key, 0)
            )

        # copy over faf95s with their own tag for each dataset
        for key in (key for key in stats["exomes"].keys() if "95" in key):
            combined[key + "_exome"] = stats["exomes"].get(key, 0)
            combined[key + "_genome"] = stats["genomes"].get(key, 0)

        # recalculate allele frequencies
        try:
            combined['AF'] = combined['AC'] / combined['AN']
        except (TypeError, ZeroDivisionError):
            combined['AF'] = 0
        for key in (key for key in combined.keys() if 'AF_' in key):
            pop = key.split('_')[1]
            try:
                combined[key] = combined['AC_' + pop] / combined['AN_' + pop]
            except (TypeError, ZeroDivisionError):
                combined[key] = 0
        return combined

    def convert_fafs(self, stats, name):
        for key in (key for key in list(stats.keys()) if "95" in key):
            stats[key + "_" + name] = stats.get(key, 0)
        return stats

    def add_hw_test_results(self, stats, gene):
        cutoff_ar, cutoff_ad = fetch_cutoffs(gene)
        for key in (key for key in list(stats.keys()) if "AC_" in key):
            dataset = key.split("_")[1]
            ac = stats[key]
            an = stats["AN_" + dataset]
            flag, flag_ar, flag_ad = hw_test(an, ac, cutoff_ar, cutoff_ad)
            stats["allelefreqtest_" + dataset] = flag
            stats["ARHWtest_" + dataset] = flag_ar
            stats["ADHWtest" + dataset] = flag_ad
        return stats


def split_ignore_empty(string, delimiter):
    if not string.strip():
        return []
    return string.split(delimiter)


def safe_add(a, b):
    if not a:
        return b
    if not b:
        return a
    return a + b
