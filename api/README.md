# GnomAD-API

[![bitbucket](https://img.shields.io/badge/bitbucket-gnomad--docker-informational.svg)](
    https://bitbucket.ambrygen.com/projects/BSTR/repos/gnomad-docker/browse)
[![portus](https://img.shields.io/badge/Portus-gnomad--api/api-informational.svg)](
    https://docker.ambrygen.com/repositories/52)
[![bamboo](https://img.shields.io/badge/bamboo-BSTR_GNMAD-informational.svg)](
    https://bamboo.ambrygen.com/browse/BSTR-GNMAD)
    
## What is it?

This docker image provides RESTful API endpoints to query genomic and exomic 
population allele frequencies, as aggregated in the Broad Institute's [gnomAD](
https://gnomad.broadinstitute.org/) database.

## How to use it?

### Start the service

The built docker image is available on [Portus](https://docker.ambrygen.com/repositories/52).
It can be run directly using `docker run` or `docker-compose up`.

#### Requirements

 * The service requires the [individual chromosome gnomAD v2 VCF and .tbi files](
https://gnomad.broadinstitute.org/downloads), which are available internally on 
`usav1isip1.ambrygenetics.local:/ifs/USAV1ISIp1/gnomad`.
 * The API queries the BioTools API to determine genomic coordinates of genes.
 * The API binds to port 8000 inside the container, on which it provides the 
RESTful endpoints. To configure the exposed port on the host with the included
`docker-compose.yml` file, define the `GNOMAD_API_PORT` environment variable.
 * By default the container runs as user `nobody`; this can be changed with the `-u`
option to `docker run`. This should be a user that has read access to the mounted 
volume. The container will refuse to run as `root`.

#### Start with `docker run`

```console
$ docker volume create -o type=nfs -o o=addr=usav1isip1.ambrygenetics.local,ro,nolock -o device=:/ifs/USAV1ISIp1/gnomad gnomad_data
$ docker run -d -v gnomad_data:/gnomad:nocopy -p 5000:8000 docker.ambrygen.com/bstr/gnomad-api/api:latest
```

#### Start with `docker-compose`

```console
$ docker-compose up -d
```

To override some of the default settings, such as exposed host port or location
of GnomAD data on the host, provide a `.env` file. See the included `.env.dist`
file and the `docker-compose.yml` file for more information.

#### Advanced configuration

See "Server configuration" below, for more options.

### Query the service

#### Single alteration

```console
$ curl localhost:5000/17/7579368/A/G
{
  "exomes":{
    "AC_afr":16,
    "AN_afr":16246,
    "AF_afr":0.000984858,
    "nhomalt_afr":0,
    "AC_amr":5,
    "AN_amr":34586,
    "AF_amr":0.000144567,
    "nhomalt_amr":0,
    "AC_asj":0,
    "AN_asj":10066,
    "AF_asj":0.0,
    "nhomalt_asj":0,
    "AC_eas":0,
    "AN_eas":18392,
    "AF_eas":0.0,
    "nhomalt_eas":0,
    "AC_fin":0,
    "AN_fin":21644,
    "AF_fin":0.0,
    "nhomalt_fin":0,
    "AC_nfe":0,
    "AN_nfe":113676,
    "AF_nfe":0.0,
    "nhomalt_nfe":0,
    "AC_oth":0,
    "AN_oth":6128,
    "AF_oth":0.0,
    "nhomalt_oth":0,
    "AC_female":11,
    "AN_female":115490,
    "AF_female":9.52463e-05,
    "nhomalt_female":0,
    "AC_male":10,
    "AN_male":135860,
    "AF_male":7.36052e-05,
    "nhomalt_male":0,
    "AC_sas":0,
    "AN_sas":30612,
    "AF_sas":0.0,
    "nhomalt_sas":0,
    "popmax":"afr",
    "AC_popmax":16,
    "AN_popmax":16246,
    "AF_popmax":0.000984858,
    "nhomalt_popmax":0,
    "nhetalt_afr":16,
    "nwtalt_afr":8107.0,
    "nhetalt_amr":5,
    "nwtalt_amr":17288.0,
    "nhetalt_asj":0,
    "nwtalt_asj":5033.0,
    "nhetalt_eas":0,
    "nwtalt_eas":9196.0,
    "nhetalt_fin":0,
    "nwtalt_fin":10822.0,
    "nhetalt_nfe":0,
    "nwtalt_nfe":56838.0,
    "nhetalt_oth":0,
    "nwtalt_oth":3064.0,
    "nhetalt_female":11,
    "nwtalt_female":57734.0,
    "nhetalt_male":10,
    "nwtalt_male":67920.0,
    "nhetalt_sas":0,
    "nwtalt_sas":15306.0
  },
  "genomes":{
    "AC_afr":12,
    "AN_afr":8702,
    "AF_afr":0.00137899,
    "nhomalt_afr":0,
    "AC_amr":1,
    "AN_amr":848,
    "AF_amr":0.00117925,
    "nhomalt_amr":0,
    "AC_asj":0,
    "AN_asj":290,
    "AF_asj":0.0,
    "nhomalt_asj":0,
    "AC_eas":0,
    "AN_eas":1560,
    "AF_eas":0.0,
    "nhomalt_eas":0,
    "AC_fin":0,
    "AN_fin":3476,
    "AF_fin":0.0,
    "nhomalt_fin":0,
    "AC_nfe":0,
    "AN_nfe":15426,
    "AF_nfe":0.0,
    "nhomalt_nfe":0,
    "AC_oth":0,
    "AN_oth":1082,
    "AF_oth":0.0,
    "nhomalt_oth":0,
    "AC_female":5,
    "AN_female":13916,
    "AF_female":0.000359299,
    "nhomalt_female":0,
    "AC_male":8,
    "AN_male":17468,
    "AF_male":0.00045798,
    "nhomalt_male":0,
    "popmax":"afr",
    "AC_popmax":12,
    "AN_popmax":8702,
    "AF_popmax":0.00137899,
    "nhomalt_popmax":0,
    "nhetalt_afr":12,
    "nwtalt_afr":4339.0,
    "nhetalt_amr":1,
    "nwtalt_amr":423.0,
    "nhetalt_asj":0,
    "nwtalt_asj":145.0,
    "nhetalt_eas":0,
    "nwtalt_eas":780.0,
    "nhetalt_fin":0,
    "nwtalt_fin":1738.0,
    "nhetalt_nfe":0,
    "nwtalt_nfe":7713.0,
    "nhetalt_oth":0,
    "nwtalt_oth":541.0,
    "nhetalt_female":5,
    "nwtalt_female":6953.0,
    "nhetalt_male":8,
    "nwtalt_male":8726.0
  }
}
```

#### Gene

All gnomAD variants in a gene can be queried by symbol or transcript.

```console
$ curl localhost:5000/MLH1
{
  "exomes": [
    {
      "chrom": "3",
      "pos": 37034992,
      "ref": "A",
      "alt": "C",
      "AC_afr": 0,
      "AN_afr": 16248,
      "AF_afr": 0.0,
      "nhomalt_afr": 0,
      "AC_amr": 0,
      "AN_amr": 34588,
      "AF_amr": 0.0,
      "nhomalt_amr": 0,
      "AC_asj": 0,
      "AN_asj": 10076,
      "AF_asj": 0.0,
      "nhomalt_asj": 0,
      "AC_eas": 0,
      "AN_eas": 18392,
      "AF_eas": 0.0,
      "nhomalt_eas": 0,
      "AC_fin": 0,
      "AN_fin": 21636,
      "AF_fin": 0.0,
      "nhomalt_fin": 0,
      "AC_nfe": 1,
      "AN_nfe": 113662,
      "AF_nfe": 8.79802e-06,
      "nhomalt_nfe": 0,
      "AC_oth": 0,
      "AN_oth": 6132,
      "AF_oth": 0.0,
      "nhomalt_oth": 0,
      "AC_female": 0,
      "AN_female": 115456,
      "AF_female": 0.0,
      "nhomalt_female": 0,
      "AC_male": 1,
      "AN_male": 135894,
      "AF_male": 7.35868e-06,
      "nhomalt_male": 0,
      "AC_sas": 0,
      "AN_sas": 30616,
      "AF_sas": 0.0,
      "nhomalt_sas": 0,
      "popmax": "nfe",
      "AC_popmax": 1,
      "AN_popmax": 113662,
      "AF_popmax": 8.79802e-06,
      "nhomalt_popmax": 0,
      "nhetalt_afr": 0,
      "nwtalt_afr": 8124.0,
      "nhetalt_amr": 0,
      "nwtalt_amr": 17294.0,
      "nhetalt_asj": 0,
      "nwtalt_asj": 5038.0,
      "nhetalt_eas": 0,
      "nwtalt_eas": 9196.0,
      "nhetalt_fin": 0,
      "nwtalt_fin": 10818.0,
      "nhetalt_nfe": 1,
      "nwtalt_nfe": 56830.0,
      "nhetalt_oth": 0,
      "nwtalt_oth": 3066.0,
      "nhetalt_female": 0,
      "nwtalt_female": 57728.0,
      "nhetalt_male": 1,
      "nwtalt_male": 67946.0,
      "nhetalt_sas": 0,
      "nwtalt_sas": 15308.0
    },
...
    {
      "chrom": "3",
      "pos": 37092309,
      "ref": "TATAA",
      "alt": "T",
      "AC_afr": 36,
      "AN_afr": 8678,
      "AF_afr": 0.00414842,
      "nhomalt_afr": 0,
      "AC_amr": 0,
      "AN_amr": 848,
      "AF_amr": 0.0,
      "nhomalt_amr": 0,
      "AC_asj": 0,
      "AN_asj": 290,
      "AF_asj": 0.0,
      "nhomalt_asj": 0,
      "AC_eas": 0,
      "AN_eas": 1558,
      "AF_eas": 0.0,
      "nhomalt_eas": 0,
      "AC_fin": 0,
      "AN_fin": 3472,
      "AF_fin": 0.0,
      "nhomalt_fin": 0,
      "AC_nfe": 0,
      "AN_nfe": 15428,
      "AF_nfe": 0.0,
      "nhomalt_nfe": 0,
      "AC_oth": 0,
      "AN_oth": 1088,
      "AF_oth": 0.0,
      "nhomalt_oth": 0,
      "AC_female": 14,
      "AN_female": 13914,
      "AF_female": 0.00100618,
      "nhomalt_female": 0,
      "AC_male": 22,
      "AN_male": 17448,
      "AF_male": 0.00126089,
      "nhomalt_male": 0,
      "popmax": "afr",
      "AC_popmax": 36,
      "AN_popmax": 8678,
      "AF_popmax": 0.00414842,
      "nhomalt_popmax": 0,
      "nhetalt_afr": 36,
      "nwtalt_afr": 4303.0,
      "nhetalt_amr": 0,
      "nwtalt_amr": 424.0,
      "nhetalt_asj": 0,
      "nwtalt_asj": 145.0,
      "nhetalt_eas": 0,
      "nwtalt_eas": 779.0,
      "nhetalt_fin": 0,
      "nwtalt_fin": 1736.0,
      "nhetalt_nfe": 0,
      "nwtalt_nfe": 7714.0,
      "nhetalt_oth": 0,
      "nwtalt_oth": 544.0,
      "nhetalt_female": 14,
      "nwtalt_female": 6943.0,
      "nhetalt_male": 22,
      "nwtalt_male": 8702.0
    }
  ]
}
``` 

## Server configuration

### Volume `/gnomad`

The service requires the [individual chromosome gnomAD v2 VCF and .tbi files](
https://gnomad.broadinstitute.org/downloads), which are available internally on 
`usav1isip1.ambrygenetics.local:/ifs/USAV1ISIp1/gnomad`. To override the source
when using `docker-compose`, define `GNOMAD_NFS_MOUNT_SERVER` (default: 
`usav1isip1.ambrygenetics.local`) and `GNOMAD_NFS_REMOTH_PATH` (default:
`/ifs/USAV1ISIp1/gnomad`).

### `gunicorn` webserver configuration

The configuration options for the `gunicorn` scalable WSGI web server which
provides the HTTP interface to the app can be found in the README for the 
[`docker.ambrygen.com/bstr/meinheld-gunicorn` base image](https://bitbucket.ambrygen.com/projects/CMPBIO/repos/meinheld-gunicorn-docker/browse).

The most relevant settings for deployment are:

 * `PORT` - The internal container port on which the server listens; because the
   container runs as a non-root user, it can only bind ports >= 1024. (default: 8000)
 * `WEB_CONCURENCY` - Number of worker processes (instances of app that the 
   server can delegate to); each worker uses a maximum of 1 CPU. (default: 2 * #CPUs on host)
 * `LOG_LEVEL` - The log level if the web server; 
   [allowed levels](https://docs.python.org/3.7/library/logging.html#levels). (default: INFO)

## Development

### Building

Build local:

```console
docker build -t gnomad .
```

Tag for Portus:

```console
docker build -t docker.ambrygen.com/bstr/gnomad-api/api:latest .
```

Tag specific version:

```console
docker build -t docker.ambrygen.com/bstr/gnomad-api/api:0.1.0 .
```

