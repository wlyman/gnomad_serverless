# Define global args
ARG FUNCTION_DIR="/home/lambda/"
ARG RUNTIME_VERSION=""
ARG DISTRO_VERSION="3.12"
ARG SERVERLESS_TAG="1.7.6"

# Stage 1 - bundle base image + runtime
# Grab a fresh copy of the image and install GCC
#FROM python:${RUNTIME_VERSION}-alpine${DISTRO_VERSION} AS python-alpine
FROM python:3.8-slim AS python-slim 


RUN echo 10.1.51.243 api.ambrygen.com >> /tmp/hosts
RUN mkdir -p -- /lib-override && cp /lib/x86_64-linux-gnu/libnss_files.so.2 /lib-override
RUN perl -pi -e 's:/etc/hosts:/tmp/hosts:g' /lib-override/libnss_files.so.2
ENV LD_LIBRARY_PATH /lib-override

#Gnomad stuff


ENV PATH=$PATH:$HOME/bin

RUN apt-get -y update && \
    apt-get -y upgrade && \
    apt-get -y install git && \
    apt-get -y install make && \
    apt-get -y install build-essential && \
    apt-get -y install zlib1g-dev && \ 
    apt-get -y install libghc-bzlib-dev && \
    apt-get -y install memcached libmemcached-tools && \
    apt-get -y install libcurl4-openssl-dev  && \
    apt-get -y install liblzma-dev && \
    apt-get -y install git


#RUN git clone https://github.com/samtools/htslib.git \
#      && cd htslib && git submodule update --init --recursive  \
#      && cd .. && make -C htslib

#RUN git clone https://github.com/samtools/bcftools.git  \
#      && make -C bcftools



RUN pip install requests python-dotenv statsmodels scipy mysql-connector-python Flask-Caching python-memcached
### conclude gnomad stuff
# ?


# Install GCC (Alpine uses musl but we compile and link dependencies with GCC)
##RUN apk add --no-cache \
##    libstdc++

# Stage 2 - build function and dependencies


# Include global args in this stage of the build
ARG FUNCTION_DIR
ARG RUNTIME_VERSION
ARG SERVERLESS_TAG

# Create function directory
RUN mkdir -p ${FUNCTION_DIR}

# Install Lambda Runtime Interface Client for Python
RUN python${RUNTIME_VERSION} -m pip install awslambdaric --target ${FUNCTION_DIR}

# Copy required files
COPY * ${FUNCTION_DIR}
COPY api ${FUNCTION_DIR}/api

# Install the function's dependencies
RUN python${RUNTIME_VERSION} -m pip install -r ${FUNCTION_DIR}/requirements.txt --target ${FUNCTION_DIR}

# Download serverless-wsgi and copy wsgi_handler.py & serverless_wsgi.py
RUN git config --global advice.detachedHead false

RUN rm -rf ${FUNCTION_DIR}serverless-wsgi
RUN git clone https://github.com/logandk/serverless-wsgi --branch ${SERVERLESS_TAG} ${FUNCTION_DIR}serverless-wsgi
RUN cp ${FUNCTION_DIR}serverless-wsgi/wsgi_handler.py ${FUNCTION_DIR}wsgi_handler.py && cp ${FUNCTION_DIR}serverless-wsgi/serverless_wsgi.py ${FUNCTION_DIR}serverless_wsgi.py

# Stage 3 - final runtime image
# Grab a fresh copy of the Python image
#FROM python:3.8-slim  

# Include global arg in this stage of the build
#ARG FUNCTION_DIR

# Set working directory to function root directory

RUN git clone https://github.com/samtools/htslib.git \
      && cd htslib && git submodule update --init --recursive  \
      && cd .. && make -C htslib

RUN git clone https://github.com/samtools/bcftools.git  \
      && make -C bcftools


WORKDIR ${FUNCTION_DIR}
# Copy in the built dependencies
#COPY --from=build-image ${FUNCTION_DIR} ${FUNCTION_DIR}

# (Optional) Add Lambda Runtime Interface Emulator and use a script in the ENTRYPOINT for simpler local runs
ADD https://github.com/aws/aws-lambda-runtime-interface-emulator/releases/latest/download/aws-lambda-rie /usr/bin/aws-lambda-rie
RUN chmod 755 /usr/bin/aws-lambda-rie
COPY entry.sh /
RUN chmod +x entry.sh

ENTRYPOINT [ "./entry.sh" ]
CMD [ "wsgi_handler.handler" ]
